#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
typedef int temp_file_handle ;
temp_file_handle write_temp_file (char* buffer, size_t length);
char* read_temp_file (temp_file_handle temp_file, size_t* length);
int main(){

	char* buffer;
	char b='k';
	buffer=&b;
	size_t l= sizeof(buffer);
	temp_file_handle devuelve=write_temp_file (buffer, l);

	printf("el indicador es : %i\n",devuelve);
	size_t* y= &devuelve;

	char* leer=read_temp_file (devuelve, y);
	printf("leido del buffer: %c \n", *leer);
	return 0;
}



temp_file_handle write_temp_file (char* buffer, size_t length)
{
	char temp_filename[] = "/tmp/temp_file.XXXXXX";
	int fd = mkstemp (temp_filename);
	unlink (temp_filename);
	write (fd, &length, sizeof (length));
	write (fd, buffer, length);
	return fd;
}
char* read_temp_file (temp_file_handle temp_file, size_t* length)
{
	char* buffer;
	int fd = temp_file;
	lseek (fd, 0, SEEK_SET);
	read (fd, length, sizeof (*length));
	buffer = (char*) malloc (*length);
	read (fd, buffer, *length);
	close (fd);
	return buffer;
}
