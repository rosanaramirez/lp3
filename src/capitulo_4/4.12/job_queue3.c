#include <malloc.h>
#include <pthread.h>
#include <semaphore.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

struct job{
    struct job* next;

    char trabajo[20];
};

struct job* job_queue;
pthread_t hilos[2];

void process_job (struct job*);
void enqueue_job(char* );

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
sem_t job_queue_count;

void initialize_job_queue(){
    //inicializar la cola
    job_queue = NULL;
    //inicializar el semaforo.
    sem_init(&job_queue_count, 0, 0);
}

void * thread_function(void* arg){
    while(1) {
        struct job* next_job;
        //espera que el valor del semaforo sea positivo para poder trabajar
        printf("Esperando un trabajo\n");
        sem_wait(&job_queue_count);

        //bloquea el mutex
        pthread_mutex_lock(&mutex);
        //pq el semafoto dio positivo, sabemos que la cola tiene trabajos por hacer
        next_job = job_queue;
        //remover de la cola
        job_queue = job_queue -> next;

        //desbloquear el mutex
        pthread_mutex_unlock(&mutex);

        process_job(next_job);
        free(next_job); 
    }
    return NULL;
}

void process_job (struct job* next_job){
    printf("%s\n",next_job->trabajo);
}


//agregar trabajos a la lista;

void enqueue_job(char* trabajo){
    struct job* new_job ;
    new_job = (struct job*)malloc(sizeof(struct job));

    strcpy(new_job->trabajo, trabajo);
    
    pthread_mutex_lock(&mutex);
    
    new_job -> next = job_queue;
    job_queue = new_job;

    sem_post(&job_queue_count);

    pthread_mutex_unlock(&mutex);
}

int main(){
    int i;
    initialize_job_queue();
    printf("creando hilos!\n");
    for(i=0;i<2;i++){
        pthread_create(&hilos[i],NULL,&thread_function,NULL);
    }
    enqueue_job("limpiando");
    enqueue_job("cocinando");
    enqueue_job("arreglando");
    enqueue_job("ordenando");
   // printf("Esperando hilos");
    sleep(5);
    exit(0);    
    for(i=0;i<2;i++){
        pthread_join(hilos[i],NULL);
    }
}
